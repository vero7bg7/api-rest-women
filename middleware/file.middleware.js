
import multer from "multer";
import path from "path";

import { fileURLToPath } from "url";
const __dirname = path.dirname(fileURLToPath(import.meta.url));

//2-Configuración del storage
const storage = multer.diskStorage({
    filename: (req, file, cb) => {
      cb(null, `${Date.now()} - ${file.originalname}`);
                //02/07/2022 - NombreFichero.png
    },
    destination: (req, file, cb) => {
      cb(null, path.join(__dirname, "../temp"));
    }
  });

  //3- Configuración del fileFilter
  const VALID_FILE_TYPES = ["image/png", "image/jpg", "image/jpeg"];

  const fileFilter = (req, file, cb) => {
    if (!VALID_FILE_TYPES.includes(file.mimetype)) {
      cb(new Error("Invalid file type"));
    } else {
      cb(null, true);
    }
  }

//1-Creo la estructura de upload
const upload = multer({
  //dónde quiero almacenar
    storage,
    //filtro de los ficheros que subamos
  fileFilter,
});

export { upload };